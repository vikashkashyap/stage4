package com.cognizant.springlearn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.validation.*;


public class Country {
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringLearnApplication.class);

@NotNull
@Size(min = 2, max = 2 ,message = "maximum length of code should be 2")
  private String code;
  private String name;
public String getName() {
	   LOGGER.info("inside the getname");

	return name;
}
public void setName(String name) {
	this.name = name;
	   LOGGER.info("inside the setname");

}
public String getCode() {
	   LOGGER.info("inside the getcode");

	return code;
}
public void setCode(String code) {
	this.code = code;
	   LOGGER.info("inside the setcode");

}
   public Country() {
	super();
	   LOGGER.info("inside the constructor");

}
   public String toString() { 
	    return "Name: '" + this.name + "', Code: '" + this.code ;
	} 
   
   
   
   
	
}
