package com.cognizant.springlearn;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapProperties.Validation;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearn.CountryService.CountryService;
import com.cognizant.springlearn.CountryService.Exception.CountryNotFoundException;

@RestController

public class CountryController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringLearnApplication.class);
   

	//ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
//    Validator validator = factory.getValidator();
//
//    // Validation is done against the annotations defined in country bean
//    Set<ConstraintViolation<Country>> violations = validator.validate(country);
//    List<String> errors = new ArrayList<String>();
//
//    // Accumulate all errors in an ArrayList of type String
//    for (ConstraintViolation<Country> violation : violations) {
//        errors.add(violation.getMessage());
//    }
//
//    // Throw exception so that the user of this web service receives appropriate error message
//    if (violations.size() > 0) {
//        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errors.toString());
//    }
//
//	
   
	CountryService countryService = new CountryService();

	@RequestMapping("/country")
	public Country getCountryIndia() {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		Country country = (Country) context.getBean("country", Country.class);
		return country;
	}
	
	@GetMapping("/countries")
	public List<Country> getAllCountries() {
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		List<Country> country =  (List<Country>)context.getBean("countryList", ArrayList.class);
		//country.get(0).g
		return country;
	}
	
	@GetMapping("/countries/{code}")
	public List<Country> getCountry(@PathVariable String code) throws CountryNotFoundException{
		
		List<Country> list =  countryService.getCountry(code);
		return list;
	}
	
	@PostMapping("/countries")
	public Country addCountry(@RequestBody @Valid Country country) {
		
		
		LOGGER.info("START addCOuntry");
		LOGGER.debug("country details"+country);
		return country;
		
		
	}
	@DeleteMapping("/countries/{code}")
	public List<Country> deletecountry(@PathVariable String code) {
		LOGGER.info("STart delete controller");
		return countryService.deleteCountry(code);
		
	}
	
//	@PutMapping("countries/{code}")
//	public List<Country> updateCountry(@PathVariable String code){
//		return countryService.updateCountry(code).Country();
//		
//	}
	
	
	

}
