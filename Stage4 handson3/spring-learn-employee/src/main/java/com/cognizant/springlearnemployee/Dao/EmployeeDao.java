package com.cognizant.springlearnemployee.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cognizant.springlearnemployee.Employee.Employee;

public class EmployeeDao {
	
	
	public EmployeeDao(){
		//this.employee_List = employees_List;
	}
	public List<Employee> getAllEmployees(){
		System.out.println("daosatart");
		ApplicationContext context = new ClassPathXmlApplicationContext("employee.xml");
      List<Employee> emp = (List<Employee>) context.getBean("employeeList", ArrayList.class);
		//LOGGER
      System.out.println("daoend");
		return emp;
	}

}
