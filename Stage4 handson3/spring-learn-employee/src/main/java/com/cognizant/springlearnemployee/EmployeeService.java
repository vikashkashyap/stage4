package com.cognizant.springlearnemployee;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cognizant.springlearnemployee.Dao.EmployeeDao;
import com.cognizant.springlearnemployee.Employee.Employee;

@Component
public class EmployeeService {
	
	//@Transactional 
	public List<Employee> getAllEmployees(){
//		.debug("AL
		System.out.println("empservice");
		
		return new EmployeeDao().getAllEmployees();
		
	}
}
