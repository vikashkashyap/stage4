package com.cognizant.loans;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoanController {
	@GetMapping("/loans/{loanNumber}")
	public Loan getLoan(@PathVariable String loanNumber){
		//List<Account> list = new ArrayList<>() ;
		ApplicationContext context = new ClassPathXmlApplicationContext("loan.xml");
		Loan loan =  context.getBean("loan", Loan.class);
		//country.get(0).g
		return loan;
		}

}
