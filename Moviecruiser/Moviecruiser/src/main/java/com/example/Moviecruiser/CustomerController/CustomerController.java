package com.example.Moviecruiser.CustomerController;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

//import com.cognizant.springlearn.Country;
import com.example.Moviecruiser.Movie;

@RestController
public class CustomerController {
	@GetMapping("/customermovie")
	public Movie  movie() {
		System.out.println("hi movie");
		ApplicationContext context = new ClassPathXmlApplicationContext("Movie.xml");
    	Movie movie = (Movie)context.getBean("movie", Movie.class);

		return movie;
	}
	@GetMapping("/activemovie")
	public Movie  active() {
		System.out.println("hi movie");
		ApplicationContext context = new ClassPathXmlApplicationContext("Movie.xml");
    	Movie movie = (Movie)context.getBean("movie", Movie.class);

		return movie;
	}
	@GetMapping("/favorite")
	public Movie  favorite() {
		System.out.println("hi movie");
		ApplicationContext context = new ClassPathXmlApplicationContext("Movie.xml");
    	Movie movie = (Movie)context.getBean("favorite", Movie.class);

		return movie;
	}

	//@DeleteMapping("/delete")
	@DeleteMapping("/favoritedelete/{title}")
	public Movie deleteFavorite(@PathVariable String title) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Movie.xml");
    	Movie movie = (Movie)context.getBean("movie", Movie.class);
		if(movie.getTitle().equals(title)) {
			return movie;
		}else {
			return null;
		}
		//LOGGER.info("STart delete controller");
		
		
	} 
	
	

}
