package com.example.Moviecruiser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoviecruiserApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviecruiserApplication.class, args);
	}

}
