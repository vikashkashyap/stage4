package com.cognizant.springlearnemployee.Controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearnemployee.EmployeeService;
import com.cognizant.springlearnemployee.Employee.Employee;

@RestController
public class EmployeeController {
	
	@GetMapping("/employees")
	public List<Employee> getAllEmployees(){
		System.out.println("employee controller start");
		return new EmployeeService().getAllEmployees();
		
	}

}
